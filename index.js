// EXPRESS SETUP

// 1. Import express by using the 'require' directive
const express = require('express')

// 2. Use the express() function and assign it to the app variable
const app = express()

// 3. Declare a variable for the port of this server
const port = 3000

// .use taga plug in 
// 4. Use these middleware to enable our server to read JSON as regular javascript
app.use(express.json())
app.use(express.urlencoded({extended: true}))


// 5. You can then have your routes after all of that
// ROUTES
// Landing page route
app.get('/', (request, response) => {
	response.send('Hello World!')
})

// Post request route
app.post('/hello', (request, response) => {
	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`)
})

// Register user route
let users = []

app.post('/register', (request, response) => {
	console.log(request.body)

	if(request.body.username !== '' && request.body.password !== ''){
		users.push(request.body)
		console.log(users)
		response.send(`User ${request.body.username} successfully registered`)
	} else {
		response.send('Please input BOTH username and password')
	}
})


// Change password route


app.put('/change-password', (request, response) => {

	// Declare a message variable that will be re-assigned a string depending on the outcome of the process
	let message

	// Loop through the whole users array (declared above) so that the server can check if the username that was inputed from the request matches any of the existing usernames inside that array
	for(let i = 0; i < users.length; i++){
		if(request.body.username == users[i].username){ //If a username matches, re-assign/change the existing user's password
			users[i].password = request.body.password

			message = `User ${request.body.username}'s password has been updated`

			break
		} else { //if no username matches, set the message variable to a string saying that the user does not exist
			message = 'User does not exist.'
		}
	}

	// Send a response with the respective message depending on the result of the condition
	response.send(message)
})

app.listen(port, () => console.log(`Server is running at port: ${port}`))