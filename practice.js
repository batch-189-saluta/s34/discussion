const express = require('express')

const app = express()

const port = 4000

app.use(express.json())
app.use(express.urlencoded({extended:true}))

// routes
app.get('/', (request,response) => {
	response.send('Hello World')
})

// post route
app.post('/hello', (request,response) => {
	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}`)
})

// register route

let users = []

app.post('/register', (request,response) => {

	if (request.body.username !== '' && request.body.password !==''){
		users.push(request.body)
		console.log(users)
		response.send(`User ${request.body.username} successfuly registered`)
	} else {
		response.send('please input BOTH username and password')
	}
})


// change password route

app.put('/change-password', (request, response) => {

	let message

	for(i=0; i<users.length; i++){
		if(request.body.username == users[i].username){
			users[i].password == request.body.password

			message = `User ${request.body.username}'s password has been updated`

			break
		} else {
		message = 'user does not exist'
		}
	} 

	response.send(message)
})



app.listen(port, () => console.log(`server is running at port: ${port}`))